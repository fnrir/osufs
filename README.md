# osu!fs

A fuse filesystem driver that lets you access files in the "files" directory (without going insane).

Currently only tested on Linux.

## Usage

Run `cargo build --release` to build this.

Then use it like any mount command, specifying the osu! directory and the
mountpoint.

(The osu dir is ~/.local/share/osu in the AppImage version and
~/.var/app/sh.ppy.osu/data/osu in the Flatpak version)

For example: `osufs $HOME/.local/share/osu /mount/point/here`

## Making .osz files

An .osz file is
[just a zip file](https://osu.ppy.sh/wiki/en/Client/File_formats/osz_%28file_format%29),
so you can just use your favorite WinRAR clone.

More info on osu! file formats
[here](https://osu.ppy.sh/wiki/en/Client/File_formats).

## MASSIVE RED FLAGS (aka TODO)

- There's no rust crate that opens MongoDB Realm databases (.realm). Due to
  this, the database has to be converted to JSON with
  [Realm Studio](https://www.mongodb.com/docs/realm/studio/install/)
  and placed in `$OSUDIR/client.json`.
- Due to the above, there's also no write support.
- `unwrap`s are used very liberally.
- Empty beatmaps/beatmap sets/skins are shown.
- [The code is held together by duct tape and spit.](https://xkcd.com/1513/)

## Credits

Massive thanks to the maintainers of the
[fuser](https://github.com/cberner/fuser), and
[any other crate](./Cargo.toml) used in this program.

(although I think `Err(ENOHERE)` should be used instead of having to do
`reply.error(ENOHERE); return`)
