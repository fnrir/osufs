use anyhow::Result;
use osufs_macros::populate;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::{Error as JsonError, Value};
use serde_with::{serde_as, DisplayFromStr};
use std::{collections::HashMap, ops::Deref, path::Path};
use uuid::Uuid;

type RealmTableIndex = HashMap<String, RefID>;
type RealmTableItemList = Vec<RefID>;
type RealmObjectTable = HashMap<String, Vec<Value>>;

#[repr(transparent)]
#[serde_as]
#[derive(Debug, Clone, Serialize, Deserialize)]
struct RefID(#[serde_as(as = "DisplayFromStr")] usize);

impl Deref for RefID {
    type Target = usize;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

trait Populate {
    type Target;
    fn populate(self, values: &[Value]) -> Self::Target;
}

impl<T> Populate for Vec<T>
where
    T: Populate,
{
    type Target = Vec<T::Target>;
    fn populate(self, values: &[Value]) -> Self::Target {
        self.into_iter().map(|e| e.populate(values)).collect()
    }
}

trait IntoSerdeValue<T> {
    fn into_serde_value(self) -> Result<Vec<T>, JsonError>
    where
        T: DeserializeOwned;
}

impl<T> IntoSerdeValue<T> for Vec<Value> {
    fn into_serde_value(self) -> Result<Vec<T>, JsonError>
    where
        T: DeserializeOwned,
    {
        self.into_iter()
            .map(|e| serde_json::from_value(e))
            .collect()
    }
}

#[inline]
fn from_vref<T>(value: &Value) -> Result<T, JsonError>
where
    T: DeserializeOwned,
{
    serde_json::from_value(value.clone())
}

#[inline]
fn v_find_parse<T>(values: &[Value], id: RefID) -> T
where
    T: DeserializeOwned,
{
    from_vref(values.get(*id).unwrap()).unwrap()
}

// TODO: Fix this shit
/*use std::iter::Map;
impl<T, U, F, B> Populate for U
where
    U: Iterator<Item = T>,
    T: Populate,
    F: FnMut(U::Item) -> B,
{
    type Target = Map<Self, F>;
    fn populate(self, values: &[Value]) -> Self::Target {
        self.map(|e: T| e.populate(values))
    }
}*/

#[derive(Debug, Clone, Serialize, Deserialize)]
#[populate(BeatmapMetadata)]
pub struct BeatmapMetadataRef {
    #[serde(rename = "Title")]
    title: RefID,
    #[serde(rename = "TitleUnicode")]
    title_unicode: Option<RefID>,
    #[serde(rename = "Artist")]
    artist: RefID,
    #[serde(rename = "ArtistUnicode")]
    artist_unicode: Option<RefID>,
    #[serde(rename = "Source")]
    source: Option<RefID>,
    #[serde(rename = "Tags")]
    tags: Option<RefID>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BeatmapMetadata {
    pub title: String,
    pub title_unicode: Option<String>,
    pub artist: String,
    pub artist_unicode: Option<String>,
    source: Option<String>,
    tags: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BeatmapRef {
    #[serde(rename = "ID")]
    id: RefID,
    #[serde(rename = "DifficultyName")]
    difficulty: RefID,
    #[serde(rename = "Metadata")]
    metadata: RefID,
    #[serde(rename = "Hash")]
    hash: RefID,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Beatmap {
    pub id: Uuid,
    difficulty: String,
    pub metadata: BeatmapMetadata,
    pub hash: String,
}

/*impl Beatmap {
    pub fn name(&self) -> String {
        format!("{} - {}", self.metadata.artist, self.metadata.title,)
    }
}*/

impl Populate for BeatmapRef {
    type Target = Beatmap;
    fn populate(self, values: &[Value]) -> Self::Target {
        let Self {
            id,
            difficulty,
            metadata,
            hash,
        } = self;
        let id: String = v_find_parse(values, id);
        let id = Uuid::parse_str(&id).unwrap();
        let difficulty = v_find_parse(values, difficulty);
        let metadata: BeatmapMetadataRef = v_find_parse(values, metadata);
        let metadata = metadata.populate(values);
        let hash = v_find_parse(values, hash);
        Self::Target {
            id,
            difficulty,
            metadata,
            hash,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[populate(File)]
pub struct FileRef {
    #[serde(rename = "Hash")]
    hash: RefID,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct File {
    pub hash: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct XFileRef {
    #[serde(rename = "File")]
    file: RefID,
    #[serde(rename = "Filename")]
    filename: RefID,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct XFile {
    #[serde(rename = "File")]
    pub file: File,
    #[serde(rename = "Filename")]
    pub filename: String,
}

impl Populate for XFileRef {
    type Target = XFile;
    fn populate(self, values: &[Value]) -> Self::Target {
        let Self { file, filename } = self;
        let file: FileRef = v_find_parse(values, file);
        let file = file.populate(values);
        let filename = v_find_parse(values, filename);
        Self::Target { file, filename }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BeatmapSetRef {
    #[serde(rename = "ID")]
    id: RefID,
    #[serde(rename = "Beatmaps")]
    beatmaps: RefID,
    #[serde(rename = "Files")]
    files: RefID,
    #[serde(rename = "Hash")]
    hash: RefID,
    #[serde(rename = "OnlineID")]
    online_id: i32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum MaybeSus<T> {
    Ok(T),
    Sus(T),
}

impl<T> From<MaybeSus<T>> for Option<T> {
    fn from(val: MaybeSus<T>) -> Self {
        use MaybeSus as E;
        match val {
            E::Ok(v) => Some(v),
            E::Sus(_) => None,
        }
    }
}

impl<T> MaybeSus<T> {
    pub fn as_ref(&self) -> MaybeSus<&T> {
        use MaybeSus as E;
        match &self {
            E::Ok(ref v) => E::Ok(v),
            E::Sus(ref v) => E::Sus(v),
        }
    }
    fn classify(value: T, is_ok: fn(&T) -> bool) -> Self {
        if is_ok(&value) {
            Self::Ok(value)
        } else {
            Self::Sus(value)
        }
    }
}

impl<T> MaybeSus<&T> {
    pub fn copied(self) -> MaybeSus<T>
    where
        T: Copy,
    {
        use MaybeSus as E;
        match self {
            E::Ok(&v) => E::Ok(v),
            E::Sus(&v) => E::Sus(v),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct BeatmapSet {
    pub id: Uuid,
    pub beatmaps: Vec<Beatmap>,
    pub files: Vec<XFile>,
    pub hash: String,
    pub online_id: MaybeSus<i32>,
}

impl Populate for BeatmapSetRef {
    type Target = BeatmapSet;
    fn populate(self, values: &[Value]) -> Self::Target {
        let Self {
            id,
            beatmaps,
            files,
            hash,
            online_id,
        } = self;
        let id = v_find_parse(values, id);
        let beatmaps: Vec<RefID> = v_find_parse(values, beatmaps);
        let beatmaps: Vec<BeatmapRef> = beatmaps
            .into_iter()
            .map(|e| v_find_parse(values, e))
            .collect();
        let beatmaps = beatmaps.populate(values);
        let files: Vec<RefID> = v_find_parse(values, files);
        let files: Vec<XFileRef> = files.into_iter().map(|e| v_find_parse(values, e)).collect();
        let files = files.populate(values);
        let hash = v_find_parse(values, hash);
        let online_id = MaybeSus::classify(online_id, |e| *e > 0);
        Self::Target {
            id,
            beatmaps,
            files,
            hash,
            online_id,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SkinRef {
    #[serde(rename = "ID")]
    id: RefID,
    #[serde(rename = "Name")]
    name: RefID,
    #[serde(rename = "Creator")]
    creator: Option<RefID>,
    #[serde(rename = "Hash")]
    hash: RefID,
    #[serde(rename = "Protected")]
    protected: bool,
    #[serde(rename = "Files")]
    files: RefID,
}

impl Populate for SkinRef {
    type Target = Skin;
    fn populate(self, values: &[Value]) -> Self::Target {
        let Self {
            id,
            name,
            creator,
            hash,
            protected,
            files,
        } = self;
        let id = v_find_parse(values, id);
        let name = v_find_parse(values, name);
        let creator = if let Some(creator) = creator {
            v_find_parse(values, creator)
        } else {
            None
        };
        let hash = v_find_parse(values, hash);
        let files: Vec<RefID> = v_find_parse(values, files);
        let files: Vec<XFileRef> = files.into_iter().map(|e| v_find_parse(values, e)).collect();
        let files = files.populate(values);
        Self::Target {
            id,
            name,
            creator,
            hash,
            protected,
            files,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Skin {
    pub id: Uuid,
    pub name: String,
    pub creator: Option<String>,
    pub hash: String,
    pub protected: bool,
    pub files: Vec<XFile>,
}

fn load_any<P: AsRef<Path>>(filename: P) -> Result<(Vec<Value>, RealmObjectTable)> {
    let file_contents = std::fs::read_to_string(filename)?;
    let random_values: Vec<Value> = serde_json::from_str(&file_contents)?;

    let table_index: RealmTableIndex = from_vref(random_values.first().unwrap())?;

    let list_of_table_names: Vec<String> = table_index.clone().into_keys().collect();

    let list_of_table_content_ids: Vec<RealmTableItemList> = table_index
        .values()
        .map(|e| v_find_parse(&random_values, e.clone()))
        .collect::<Vec<_>>()
        .into_serde_value()?;

    let meta_table: RealmObjectTable = list_of_table_names
        .into_iter()
        .zip(list_of_table_content_ids)
        .map(|(name, value)| {
            (
                name,
                value
                    .into_iter()
                    .map(|e| v_find_parse(&random_values, e))
                    .collect(),
            )
        })
        .collect();

    Ok((random_values, meta_table))
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OsuClient {
    pub beatmaps: Vec<Beatmap>,
    pub beatmap_sets: Vec<BeatmapSet>,
    pub files: Vec<File>,
    pub skins: Vec<Skin>,
}

impl OsuClient {
    pub fn open<P: AsRef<Path>>(filename: P) -> Result<Self> {
        let (values, mut meta_table) = load_any(filename)?;

        let beatmaps = meta_table.remove("Beatmap").unwrap();
        let beatmaps: Vec<BeatmapRef> = beatmaps.into_serde_value()?;
        let beatmaps = beatmaps.populate(&values);
        let _beatmap_collections = meta_table.remove("BeatmapCollection").unwrap();
        let _beatmap_metadatas = meta_table.remove("BeatmapMetadata").unwrap();
        let beatmap_sets = meta_table.remove("BeatmapSet").unwrap();
        let beatmap_sets: Vec<BeatmapSetRef> = beatmap_sets.into_serde_value()?;
        let beatmap_sets = beatmap_sets.populate(&values);
        let files = meta_table.remove("File").unwrap();
        let files: Vec<FileRef> = files.into_serde_value()?;
        let files = files.populate(&values);
        let skins = meta_table.remove("Skin").unwrap();
        let skins: Vec<SkinRef> = skins.into_serde_value()?;
        let skins = skins.populate(&values);

        Ok(Self {
            beatmaps,
            beatmap_sets,
            files,
            skins,
        })
    }
}
