use crate::{error::OsuFSError, realmdump::*};
use anyhow::Result;
use fuser::{FileAttr, FileType};
use intmap::IntMap;
use nix::errno::Errno::*;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use once_cell::sync::Lazy;
use std::{
    borrow::Borrow,
    collections::HashMap,
    ffi::OsStr,
    fmt::Debug,
    fs::File,
    hash::Hash,
    io::{BufReader, Read},
    ops::RangeInclusive,
    path::{Path, PathBuf},
    time::UNIX_EPOCH,
};
use strum::{EnumString, IntoStaticStr};
use thiserror::Error;
use tracing::{instrument, trace, warn};
use uuid::Uuid;

const FILE_PERM: u16 = 0o664;
const DIR_PERM: u16 = 0o755;
const RDEV: u32 = 0;
const BLOCK_SIZE: u32 = 512;

static UID: Lazy<nix::unistd::Uid> = Lazy::new(nix::unistd::getuid);
static GID: Lazy<nix::unistd::Gid> = Lazy::new(nix::unistd::getgid);

type Sha256Hash = String;
type Inode = u64;
type OsResult<T> = Result<T, OsuFSError>;
type DirList = Vec<(Inode, FileType, String)>;

/*
trait SafeInsert<K, V> {
    fn safe_insert<Q>(&mut self, k: K, v: V) -> Option<V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq;
}

impl<K, V, S> SafeInsert<K, V> for HashMap<K, V, S> {
    fn safe_insert<Q>(&mut self, k: K, v: V) -> Option<V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.
    }
}
*/

trait GenAttr {
    fn gen_attr(&self, ino: Inode) -> OsResult<FileAttr>;
}

impl GenAttr for Path {
    fn gen_attr(&self, ino: Inode) -> OsResult<FileAttr> {
        let file = File::open(self)?;
        let metadata = file.metadata()?;
        let atime = metadata.accessed()?;
        let mtime = metadata.modified()?;
        let perm = if metadata.is_dir() {
            DIR_PERM
        } else {
            FILE_PERM
        };
        let kind = if metadata.is_file() {
            FileType::RegularFile
        } else if metadata.is_dir() {
            FileType::Directory
        } else {
            FileType::CharDevice
        };
        Ok(FileAttr {
            ino,
            size: metadata.len(),
            blocks: 0,
            atime,
            mtime,
            ctime: UNIX_EPOCH,
            crtime: UNIX_EPOCH,
            kind,
            perm,
            nlink: 1,
            uid: UID.as_raw(),
            gid: GID.as_raw(),
            rdev: RDEV,
            blksize: BLOCK_SIZE,
            flags: 0,
        })
    }
}

trait FakeAttr {
    fn fake_attr(&self) -> FileAttr;
}

impl FakeAttr for Inode {
    fn fake_attr(&self) -> FileAttr {
        FileAttr {
            ino: *self,
            size: 0,
            blocks: 0,
            atime: UNIX_EPOCH,
            mtime: UNIX_EPOCH,
            ctime: UNIX_EPOCH,
            crtime: UNIX_EPOCH,
            kind: FileType::Directory,
            perm: DIR_PERM,
            nlink: 2,
            uid: UID.as_raw(),
            gid: GID.as_raw(),
            rdev: RDEV,
            blksize: BLOCK_SIZE,
            flags: 0,
        }
    }
}

#[derive(Debug, Clone, Copy, FromPrimitive, EnumString, IntoStaticStr)]
enum VfsSpecial {
    #[strum(disabled)]
    Root = 1,
    #[strum(serialize = "byBeatmap")]
    ByBeatmap,
    #[strum(serialize = "byBeatmapSet")]
    ByBeatmapSet,
    #[strum(serialize = "byHash")]
    ByHash,
    Skins,
    Songs,
}

#[derive(Debug, Error)]
#[error("inode is not a special directory")]
struct NotASpecialDirError;

impl TryFrom<Inode> for VfsSpecial {
    type Error = NotASpecialDirError;
    fn try_from(value: Inode) -> Result<Self, Self::Error> {
        FromPrimitive::from_u64(value).ok_or(NotASpecialDirError)
    }
}

impl VfsSpecial {
    #[inline]
    fn max() -> Inode {
        Self::Songs as Inode
    }
    #[inline]
    fn every() -> RangeInclusive<Inode> {
        Self::Root as Inode..=Self::max()
    }
    #[inline]
    fn every_subdir() -> RangeInclusive<Inode> {
        Self::ByBeatmap as Inode..=Self::max()
    }
    #[inline]
    fn name(&self) -> &'static str {
        (*self).into()
    }
    #[inline]
    fn inode(&self) -> Inode {
        *self as Inode
    }
    #[inline]
    fn attr(&self) -> FileAttr {
        self.inode().fake_attr()
    }
}

#[derive(Debug, Clone)]
struct VFile {
    inode: Inode,
    filename: String,
    hash: Sha256Hash,
}

#[derive(Debug, Clone)]
enum VfsInode {
    SpecialDir(VfsSpecial),
    RealFile(Sha256Hash),
    VirtualDir(Vec<VFile>),
}

#[inline]
fn gen_skin_file_list(hash_inodes: &HashMap<Sha256Hash, Inode>, skin: &Skin) -> Vec<VFile> {
    skin.files
        .iter()
        .map(|e| VFile {
            inode: *hash_inodes.get(&e.file.hash).unwrap(),
            filename: e.filename.clone(),
            hash: e.file.hash.to_owned(),
        })
        .collect()
}

#[inline]
fn gen_song_file_list(hash_inodes: &HashMap<Sha256Hash, Inode>, bms: &BeatmapSet) -> Vec<VFile> {
    bms.files
        .iter()
        .map(|e| VFile {
            inode: *hash_inodes.get(&e.file.hash).unwrap(),
            filename: e.filename.clone(),
            hash: e.file.hash.to_owned(),
        })
        .collect()
}

#[inline]
fn skin_name(skin: &Skin) -> Option<String> {
    let id = skin.id.to_string();
    let name = &skin.name;
    if let Some(creator) = &skin.creator {
        Some(format!("{} {} - {}", id, creator, name))
    } else {
        Some(format!("{} {}", id, name))
    }
}

#[inline]
fn song_name(bms: &BeatmapSet, original_metadata: bool) -> Option<String> {
    let Some(fmeta) = bms.beatmaps.last().map(|e| &e.metadata) else {
        warn!("Beatmap set {} seems empty. Skipping.", bms.id);
        return None;
    };
    let id: Option<i32> = bms.online_id.as_ref().copied().into();
    let id = id.map(|e| e.to_string()).unwrap_or(bms.id.to_string());
    // TODO: Some beatmaps provide empty original metadata, resulting in empty filenames
    let (artist, title) = if original_metadata {
        (
            fmeta.artist_unicode.as_ref().unwrap_or(&fmeta.artist),
            fmeta.title_unicode.as_ref().unwrap_or(&fmeta.title),
        )
    } else {
        (&fmeta.artist, &fmeta.title)
    };
    Some(format!("{} {} - {}", id, artist, title))
}

#[inline]
fn stable_add(xinodes: &mut HashMap<String, Inode>, name: String, ino: Inode) {
    // If you return a name with a backslash, you'll get an EIO error
    let name = name.replace('/', "[SLASH]");
    if xinodes.contains_key(&name) {
        panic!("Inserting {} failed!", name);
    }
    xinodes.insert(name, ino);
}

fn lookup_and_attr<K, Q: ?Sized>(name_inodes: &HashMap<K, Inode>, name: &Q) -> OsResult<FileAttr>
where
    K: Borrow<Q> + Hash + Eq,
    Q: Hash + Eq,
{
    name_inodes
        .get(name)
        .map(|e| e.fake_attr())
        .ok_or(ENOENT.into())
}

fn dirimap_to_dirlist<T: ToString>(dirimap: &HashMap<T, u64>) -> DirList {
    dirimap
        .iter()
        .map(|(name, ino)| (*ino, FileType::RegularFile, name.to_string()))
        .collect()
}

#[derive(Debug)]
pub struct OsuFS {
    path: PathBuf,
    inodemap: IntMap<VfsInode>,
    hash_inodes: HashMap<Sha256Hash, Inode>,
    beatmap_id_inodes: HashMap<Uuid, Inode>,
    beatmap_set_inodes: HashMap<Uuid, Inode>,
    // TODO: Add bySkin folder
    _skin_id_inodes: HashMap<Uuid, Inode>,
    skins_inodes: HashMap<String, Inode>,
    songs_inodes: HashMap<String, Inode>,
}

#[derive(Debug, Clone)]
struct Counter(u64);

impl Counter {
    fn new(inital: u64) -> Self {
        Self(inital)
    }
    fn iget(&mut self) -> u64 {
        let res = self.0;
        self.0 += 1;
        res
    }
}

impl OsuFS {
    #[instrument]
    pub fn new<P: AsRef<Path> + Debug>(path: P, original_metadata: bool) -> Result<Self> {
        let path = path.as_ref().to_path_buf();
        let OsuClient {
            beatmaps,
            beatmap_sets,
            files,
            skins,
        } = OsuClient::open(path.join("client.json"))?;
        let mut counter = Counter::new(VfsSpecial::max() + 1);
        let mut inodemap = IntMap::new();
        VfsSpecial::every().for_each(|k| {
            let v = VfsInode::SpecialDir(VfsSpecial::try_from(k).unwrap());
            let _ = inodemap.insert(k, v);
        });
        let hash_inodes: HashMap<Sha256Hash, Inode> = files
            .into_iter()
            .map(|e| {
                let ino = counter.iget();
                let hash = e.hash;
                let _ = inodemap.insert(ino, VfsInode::RealFile(hash.clone()));
                (hash, ino)
            })
            .collect();
        let mut songs_inodes = HashMap::new();
        let beatmap_set_inodes = beatmap_sets
            .into_iter()
            .map(|e| {
                let ino = counter.iget();
                let file_list = gen_song_file_list(&hash_inodes, &e);
                let _ = inodemap.insert(ino, VfsInode::VirtualDir(file_list));
                if let Some(name) = song_name(&e, original_metadata) {
                    stable_add(&mut songs_inodes, name, ino);
                };
                (e.id, ino)
            })
            .collect();
        let beatmap_id_inodes = beatmaps
            .into_iter()
            .map(|e| (e.id, *hash_inodes.get(&e.hash).unwrap()))
            .collect();
        let mut skins_inodes = HashMap::new();
        let skin_id_inodes = skins
            .into_iter()
            .map(|e| {
                let ino = counter.iget();
                let file_list = gen_skin_file_list(&hash_inodes, &e);
                let _ = inodemap.insert(ino, VfsInode::VirtualDir(file_list));
                if let Some(name) = skin_name(&e) {
                    stable_add(&mut skins_inodes, name, ino)
                }
                (e.id, ino)
            })
            .collect();
        trace!(
            "Finished initializing osu!fs filesystem with inode count {}",
            counter.0
        );
        Ok(Self {
            path,
            inodemap,
            hash_inodes,
            beatmap_id_inodes,
            beatmap_set_inodes,
            _skin_id_inodes: skin_id_inodes,
            songs_inodes,
            skins_inodes,
        })
    }
    #[instrument(skip(self))]
    fn hash_to_filename(&self, hash: &str) -> PathBuf {
        self.path
            .join("files")
            .join(hash.chars().take(1).collect::<String>())
            .join(hash.chars().take(2).collect::<String>())
            .join(hash)
    }
    fn xlookup_root(&self, name: &str) -> OsResult<FileAttr> {
        name.parse::<VfsSpecial>()
            .map(|e| e.attr())
            .map_err(|_| ENOENT.into())
    }
    fn xlookup_bybeatmap(&self, name: &str) -> OsResult<FileAttr> {
        let uuid = Uuid::parse_str(name).map_err(|_| ENOENT)?;
        let ino = *self.beatmap_id_inodes.get(&uuid).ok_or(ENOENT)?;
        let VfsInode::RealFile(hash) = self.inodemap.get(ino).ok_or(ENOENT)? else {
            return Err(ENOENT.into());
        };
        self.hash_to_filename(hash).gen_attr(ino)
    }
    fn xlookup_bybeatmapset(&self, name: &str) -> OsResult<FileAttr> {
        let uuid = Uuid::parse_str(name).map_err(|_| ENOENT)?;
        lookup_and_attr(&self.beatmap_set_inodes, &uuid)
    }
    fn xlookup_hash(&self, hash: &str) -> OsResult<FileAttr> {
        let ino = self.hash_inodes.get(hash).copied().ok_or(ENOENT)?;
        self.hash_to_filename(hash).gen_attr(ino)
    }
    fn xlookup_special(&self, vs: &VfsSpecial, name: &str) -> OsResult<FileAttr> {
        use VfsSpecial as E;
        match vs {
            E::Root => self.xlookup_root(name),
            E::ByBeatmap => self.xlookup_bybeatmap(name),
            E::ByBeatmapSet => self.xlookup_bybeatmapset(name),
            E::ByHash => self.xlookup_hash(name),
            E::Skins => lookup_and_attr(&self.skins_inodes, name),
            E::Songs => lookup_and_attr(&self.songs_inodes, name),
        }
    }
    fn xlookup_virtual_dir(&self, file_list: &[VFile], name: &str) -> OsResult<FileAttr> {
        file_list
            .iter()
            .find(|e| e.filename == name)
            .ok_or(ENOENT.into())
            .and_then(|e| self.hash_to_filename(&e.hash).gen_attr(e.inode))
    }
    pub fn xlookup(&mut self, parent: Inode, name: &OsStr) -> OsResult<FileAttr> {
        let name = name.to_str().unwrap();
        use VfsInode as E;
        match self.inodemap.get(parent).ok_or(ENOENT)? {
            E::SpecialDir(vs) => self.xlookup_special(vs, name),
            E::RealFile(_) => Err(ENOTDIR.into()),
            E::VirtualDir(file_list) => self.xlookup_virtual_dir(file_list, name),
        }
    }
    pub fn xgetattr(&mut self, ino: Inode) -> OsResult<FileAttr> {
        use VfsInode as E;
        match self.inodemap.get(ino).ok_or(ENOENT)? {
            E::SpecialDir(vs) => Ok(vs.attr()),
            E::RealFile(hash) => self.hash_to_filename(hash).gen_attr(ino),
            E::VirtualDir(_) => Ok(ino.fake_attr()),
        }
    }
    pub fn xread(&mut self, ino: Inode, offset: i64, size: u32) -> OsResult<Vec<u8>> {
        use VfsInode as E;
        let E::RealFile(hash) = self.inodemap.get(ino).ok_or(ENOENT)? else {
            return Err(EISDIR.into());
        };
        let filename = self.hash_to_filename(hash);
        let file = File::open(filename)?;
        let mut buf = vec![0; size as usize];
        let mut reader = BufReader::new(file);
        reader.seek_relative(offset)?;
        // ERROR HANDLING: Apparently this error has to be ignored
        let _ = reader.read_exact(&mut buf);
        Ok(buf)
    }
    fn xreaddir_root(&self) -> DirList {
        VfsSpecial::every_subdir()
            .map(|e| {
                (
                    e,
                    FileType::Directory,
                    VfsSpecial::try_from(e).unwrap().name().to_owned(),
                )
            })
            .collect()
    }
    fn xreaddir_byhash(&self) -> DirList {
        // TODO
        self.inodemap
            .iter()
            .filter_map(|(k, v)| {
                let VfsInode::RealFile(v) = v else {
                    return None;
                };
                Some((k, v))
            })
            .map(|(k, v)| (*k, FileType::RegularFile, v.to_owned()))
            .collect()
    }
    fn xreaddir_special(&self, vs: &VfsSpecial) -> DirList {
        use VfsSpecial as E;
        match vs {
            E::Root => self.xreaddir_root(),
            E::ByBeatmap => dirimap_to_dirlist(&self.beatmap_id_inodes),
            E::ByBeatmapSet => dirimap_to_dirlist(&self.beatmap_set_inodes),
            E::ByHash => self.xreaddir_byhash(),
            E::Skins => dirimap_to_dirlist(&self.skins_inodes),
            E::Songs => dirimap_to_dirlist(&self.songs_inodes),
        }
    }
    fn xreaddir_virtual_dir(&self, file_list: &[VFile]) -> DirList {
        file_list
            .iter()
            .map(|e| (e.inode, FileType::RegularFile, e.filename.to_owned()))
            .collect()
    }
    pub fn xreaddir(&mut self, ino: Inode) -> OsResult<DirList> {
        let item = self.inodemap.get(ino).ok_or(ENOENT)?;
        use VfsInode as E;
        let mut res = match item {
            E::SpecialDir(vs) => self.xreaddir_special(vs),
            E::RealFile(_) => return Err(ENOTDIR.into()),
            E::VirtualDir(file_list) => self.xreaddir_virtual_dir(file_list),
        };
        // TODO: Fix this
        //let parent = if ino == 1 { ino } else { ino };
        res.extend(vec![
            (ino, FileType::Directory, ".".to_owned()),
            (ino, FileType::Directory, "..".to_owned()),
        ]);
        Ok(res)
    }
}
