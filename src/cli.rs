use clap::Parser;
use std::path::PathBuf;

#[derive(Debug, Parser)]
pub struct Cli {
    /// For Linux the osu dir is ~/.local/share/osu in the AppImage version and
    /// ~/.var/app/sh.ppy.osu/data/osu in the Flatpak version
    pub osu_path: PathBuf,
    pub mount_point: PathBuf,
    /// Prefer metadata in original language
    #[arg(short = 'm', long)]
    pub original_metadata: bool,
}

pub fn cli() -> Cli {
    Cli::parse()
}
