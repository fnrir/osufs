use fuser::Request;
use nix::errno::Errno;
use std::io::Error as IoError;
use thiserror::Error;
use tracing::warn;

#[derive(Debug, Error)]
pub enum OsuFSError {
    #[error(transparent)]
    LibcError(#[from] Errno),
    #[error(transparent)]
    IoError(#[from] IoError),
}

impl OsuFSError {
    #[inline]
    pub fn into_i32(self, req: &Request<'_>) -> nix::libc::c_int {
        use OsuFSError as E;
        match self {
            E::LibcError(error) => {
                (match error {
                    // Due to `assert_ne!(err, 0);` we cannot send a 0, so we send an EIO instead
                    Errno::UnknownErrno => {
                        warn!(
                            "Unknown error code on request {}. Defaulting to EIO.",
                            req.unique()
                        );
                        Errno::EIO
                    }
                    error => error,
                }) as i32
            }
            E::IoError(error) => {
                if let Some(res) = error.raw_os_error() {
                    return res;
                };
                warn!(
                    "Couldn't get raw os error code of error {}. Defaulting to EIO.",
                    error
                );
                Errno::EIO as i32
            }
        }
    }
}
