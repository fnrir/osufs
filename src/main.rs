use anyhow::Result;
use fuser::{mount2, MountOption};
use std::{path::Path, process::Command};
use tracing::metadata::LevelFilter;
use tracing_subscriber::prelude::*;
use tracing_subscriber::util::SubscriberInitExt;

mod cli;
mod driver;
mod driver_impl;
mod error;
mod realmdump;

use driver::OsuFS;

pub const APP_DEBUG: bool = cfg!(debug_assertions);

fn ctrlc_umount<P: AsRef<Path>>(target: P) -> Result<()> {
    let target = target.as_ref();

    if let Ok(()) = nix::mount::umount(target) {
        return Ok(());
    };
    // NOTE: On some systems umount is a suid binary
    let res = Command::new("umount").arg(target).status()?;
    if !res.success() {
        anyhow::bail!("Error running umount: {}", res)
    }
    Ok(())
}

fn main() -> Result<()> {
    let args = cli::cli();
    let stdout = tracing_subscriber::fmt::layer()
        .pretty()
        .with_filter(if APP_DEBUG {
            LevelFilter::TRACE
        } else {
            LevelFilter::INFO
        });
    tracing_subscriber::registry().with(stdout).init();
    let path = args.mount_point.clone();
    ctrlc::set_handler(move || drop(ctrlc_umount(path.clone())))
        .expect("Error setting Ctrl-C handler");
    mount2(
        OsuFS::new(&args.osu_path, args.original_metadata)?,
        args.mount_point,
        &[
            MountOption::FSName(args.osu_path.to_str().unwrap().to_owned()),
            MountOption::NoDev,
            MountOption::NoSuid,
            MountOption::RO,
            MountOption::NoExec,
            MountOption::NoAtime,
        ],
    )?;
    Ok(())
}
