use crate::driver::OsuFS;
use fuser::{Filesystem, Request};
use std::{ffi::OsStr, time::Duration};
use tracing::{instrument, warn};

const TTL: Duration = Duration::from_secs(1);

impl Filesystem for OsuFS {
    #[instrument(skip(self, req, reply))]
    fn lookup(&mut self, req: &Request<'_>, parent: u64, name: &OsStr, reply: fuser::ReplyEntry) {
        match self.xlookup(parent, name) {
            Ok(attr) => reply.entry(&TTL, &attr, 0),
            Err(err) => reply.error(err.into_i32(req)),
        }
    }
    #[instrument(skip(self, req, reply))]
    fn getattr(&mut self, req: &Request<'_>, ino: u64, reply: fuser::ReplyAttr) {
        match self.xgetattr(ino) {
            Ok(attr) => reply.attr(&TTL, &attr),
            Err(err) => reply.error(err.into_i32(req)),
        }
    }
    #[instrument(skip(self, req, reply))]
    fn read(
        &mut self,
        req: &Request,
        ino: u64,
        _fh: u64,
        offset: i64,
        size: u32,
        _flags: i32,
        _lock: Option<u64>,
        reply: fuser::ReplyData,
    ) {
        match self.xread(ino, offset, size) {
            Ok(buf) => reply.data(&buf),
            Err(err) => reply.error(err.into_i32(req)),
        }
    }
    #[instrument(skip(self, req, reply))]
    fn readdir(
        &mut self,
        req: &Request<'_>,
        ino: u64,
        _fh: u64,
        offset: i64,
        mut reply: fuser::ReplyDirectory,
    ) {
        // TODO: Pass offset to xread
        let entries = match self.xreaddir(ino) {
            Ok(entries) => entries,
            Err(err) => {
                reply.error(err.into_i32(req));
                return;
            }
        };

        for (i, entry) in entries.into_iter().enumerate().skip(offset as usize) {
            if reply.add(entry.0, (i + 1) as i64, entry.1, entry.2) {
                break;
            }
        }
        reply.ok();
    }
}
