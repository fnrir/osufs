use proc_macro::{TokenStream, TokenTree};
use proc_macro2::{TokenStream as TokenStream2, Group, Ident, Span};
use quote::{quote, ToTokens};
use syn::{Data, DeriveInput, Fields, Type};

/*use std::process::{Command, Stdio};
use std::io::Write;
fn pretty_print(tokens: impl ToTokens) {
    let tokens = tokens.into_token_stream().to_string();

          
    let mut child = Command::new("rustfmt")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().unwrap();

          
    let mut stdin = child.stdin.take().unwrap();
    write!(stdin, "{tokens}").unwrap();
    stdin.flush().unwrap();
    drop(stdin);
    let child = child.wait_with_output().unwrap();
    let stdout = child.stderr;
    eprintln!("{}", String::from_utf8_lossy(&stdout));
    let stdout = child.stdout;
    eprintln!("{}", String::from_utf8_lossy(&stdout));
}*/

#[proc_macro_attribute]
pub fn populate(args: TokenStream, input: TokenStream) -> TokenStream {
    let ident_x = args.into_iter().take(1).last().unwrap();
    let TokenTree::Ident(ident_x) = ident_x else {
        panic!()
    };
    let ident_x = Ident::new(&ident_x.to_string(), Span::call_site());

    let ast: DeriveInput = syn::parse(input.clone()).unwrap();
    let ident = ast.ident;
    let Data::Struct(only_one) = &ast.data else {
        panic!("Not a struct!");
    };
    let Fields::Named(fields) = &only_one.fields else {
        panic!("Not a tuple struct!");
    };

    let mut field_unpack_x = TokenStream2::new();
    let mut field_tf = TokenStream2::new();
    let mut field_repack_x = TokenStream2::new();

    for field in &fields.named {
        let name = field.ident.as_ref().unwrap();
        let Type::Path(x) = &field.ty else {
            panic!();
        };
        field_unpack_x.extend(quote!{ #name, });
        match x.path.segments[0].ident.to_string().as_str() {
            "RefID" => {
                field_tf.extend(quote! {
                    let #name = serde_json::from_value(values.get(*#name).unwrap().clone()).unwrap();
                });
            },
            "Option" => {
                field_tf.extend(quote! {
                    let #name = if let Some(#name) = #name {
                        serde_json::from_value(values.get(*#name).unwrap().clone()).unwrap()
                    } else {
                        None
                    };
                });
            },
            _ => (),
        }
        field_repack_x.extend(quote!{ #name, });
    }

    let mut field_unpack = TokenStream2::new();
    field_unpack.extend(quote!{ let Self });
    field_unpack.extend(Group::new(proc_macro2::Delimiter::Brace, field_unpack_x).to_token_stream());
    field_unpack.extend(quote!{ = self; });

    let mut field_repack = TokenStream2::new();
    field_repack.extend(quote!{ Self::Target });
    field_repack.extend(Group::new(proc_macro2::Delimiter::Brace, field_repack_x).to_token_stream());

    let mut res = TokenStream2::from(input);
    let b = quote! {
        impl Populate for #ident {
            type Target = #ident_x;
            fn populate(self, values: &[Value]) -> Self::Target {
                #field_unpack
                #field_tf
                #field_repack
            }
        }
    };
    //pretty_print(b.clone());
    res.extend(b);
    res.into()
}
